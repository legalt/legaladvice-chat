/*
 LegAlt - Chat handler on webSockets
*/

var io = require('socket.io')(3000); //listenner port for socket
var _ = require('lodash');
// var mongoose    = require('mongoose');  package for noSQL db 

var aUsers = {}, //all user list
	numUsers = 0; //Count of users

//var Chat = require('./modules/Chat')(io);

/*
TODO:
-------------------------------
DB:

Need add class for handle of DB(noSQL) with Mongoose package
Set schema for users list and messages
Read|Write users
Read|Write messages
Portion load messages
________________________________
Chat:

Need add status for message (read,not read);
Sound alert if user not on this page
Portion load messages
*/

var handleChat = function(socket){	
	this.socket = socket;
};
handleChat.prototype.joinUser = function(socket,model){
	var __oUsers = {};
	var isUpdate = false;

	for(var i in aUsers){
		if(aUsers[i].id === model.id){
			isUpdate = true;
			console.log('User is update');
			delete aUsers[i];
		}
	}
	if (!isUpdate){
		numUsers++;
	}
	aUsers[socket.id] = model;
	this.actionLog('Joined new user: ' + JSON.stringify(model));
	this.sendList(socket);	
};
handleChat.prototype.sendList = function(socket){
	var __aUsers = [];
	var self = this;
	var newList = {};

	_.forEach(aUsers,function(user,key){		
		newList[key] = user;
	});
	//TODO: fix send list without current user
	/*if (newList[socket.id]){
		delete newList[socket.id];
		console.log('delete you from list');
	}	*/
	_.forEach(newList,function(val,key){
		__aUsers.push(val);
	});
	_.forEach(aUsers,function(user,socketId){
		io.sockets.connected[socketId].emit('userList',__aUsers);
	});
	this.actionLog('Send list: ' + JSON.stringify(__aUsers));
};
handleChat.prototype.sendMessage = function(users,message,socket){
	var self = this;		
	if (users.length && typeof users === 'object'){
		var _aUsers = _.unique(users);
		_.forEach(_aUsers,function(userId,key){
			var index = _.findIndex(aUsers,{id:userId});
			for(var i in aUsers){
				if (aUsers[i].id === userId){
					io.sockets.connected[i].emit('newMessage',{
						to:users,
						message:message
					});
				}
			}
		});
		this.actionLog('newMessage for specify users: ' + JSON.stringify(users));
	}else{
		this.actionLog('newMessage for all users: ' + message);
		socket.broadcast.emit('newMessage',{
			to:['All'],
			message:message
		});
	}	
};
handleChat.prototype.disconnectUser = function(socket){
	var __oUsers = aUsers;
	var self = this;
	if (__oUsers[socket.id]){
		this.actionLog('User is disconnected: ' + __oUsers[self.socket.id].id);
		delete __oUsers[socket.id];
		numUsers--;
		aUsers = __oUsers;
		self.sendList(socket);
	}
};
handleChat.prototype.exceptionError = function(){

};
handleChat.prototype.actionLog = function(msg){
	console.log(msg);
};


io.on('connection', function (socket) {  
  var chat = new handleChat(socket);		
  socket.on('join',function(model){ //Listener for join user
  	chat.joinUser(socket,model);  	  
  });
  socket.on('message',function(data){ //listener for get message and send
  	 chat.sendMessage(data.to,data.message,socket); 	
  });
  socket.on('disconnect',function(){ //disconnect user and remove with list	
	chat.disconnectUser(socket);	
  });
});